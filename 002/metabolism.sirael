/**
 * disassembled Siral metabolism model for in-silico patient simulation
 * Copyright (c) since 2020 Tomas Koutny.
 *
 * Contact:
 * tomas@sirael.science
 * Tomas Koutny
 *
 *
 * Purpose of this software:
 * This software is a personal, research project of Tomas Koutny. It is strictly
 * prohibited to use this software for diagnosis or treatment of any medical condition,
 * without proving a safety for the intended use.
 *
 * By using this software, you are responsible for:
 *  a) implementing all safety requirements,
 *  b) proving the safety for a particular purpose,
 *  c) obtaining all regulatory approvals,
 *  d) any arising harm.
 *
 * Especially, a diabetic patient is warned that improper use of this software
 * may result into severe injure, including death.
 *
 *
 *
 * Licensing terms:
 * This software is granted under the Apache 2.0 license for a non-commercial use.
 * This software can be granted for a commercial use under a written agreement only.
 * Always, you must include and display this header-comment when redistributing the software in any form.
 *
 * When referring to this work, use the following citation:
 * Koutny, T. Sirael: Virtual Metabolic Machine. Biomedical Materials & Devices (2024).
 * Available at
 *     https://link.springer.com/article/10.1007/s44174-024-00199-9
 *  or https://doi.org/10.1007/s44174-024-00199-9
 *  or git: https://gitlab.com/sirael_metabolic_simulator/
 *
 * The available dataset can be limited by its respective permissions:
 *   The source of the data is the "BIG IDEAs Lab Glycemic Variability and Wearable Device Data" dataset,
 *   but the analyses, content and conclusions presented herein are solely the responsibility of the authors
 *   and have not been reviewed or approved by the study authors.
 *
 *  For further details, see
 *    Cho, P., Kim, J., Bent, B., & Dunn, J. (2023). BIG IDEAs Lab Glycemic Variability and Wearable Device Data (version 1.1.2). PhysioNet. https://doi.org/10.13026/zthx-5212.
 *    Bent, B., Cho, P.J., Henriquez, M. et al. Engineering digital biomarkers of interstitial glucose from noninvasive smartwatches. npj Digit. Med. 4, 89 (2021). https://doi.org/10.1038/s41746-021-00465-w
 *    Goldberger, A., Amaral, L., Glass, L., Hausdorff, J., Ivanov, P. C., Mark, R., Stanley, H. E. (2000). PhysioBank, PhysioToolkit, and PhysioNet: Components of a new research resource for complex physiologic signals. Circulation [Online]. 101 (23), pp. e215–e220.
 *
 * This file was authored on 2024-09-16 19:31:01.6789640 UTC
 */

.substances begin
	fat
	glucose
	metabolic_need
	protein
	substance_01
	substance_02
	substance_03
	substance_04
	substance_05
.end substances

.compartments begin
	compartment_01
	compartment_02
	digestion
	sensor_tissue
.end compartments

.metabolism begin
	active transport: glucose in digestion -> compartment_01	(.p=58.2598 .max_rate=4795.12)
	active transport: protein in digestion -> compartment_01	(.p=12.431 .max_rate=9771.42)
	active transport: fat in digestion -> compartment_01	(.p=1.92621 .max_rate=13041.4)
	       transport: glucose in sensor_tissue -> dev_null	(.p=65.7366 .max_rate=0.0010001)
	           react: glucose & substance_01 -> substance_05 in sensor_tissue	(.p=149.586 .max_rate=324007 .ratio=177.171 .product_fraction=0.303254)
	           react: glucose & substance_01 -> substance_03 in compartment_02	(.p=147.904 .max_rate=345600 .ratio=157.253 .product_fraction=0.0166806)
	           react: substance_03 & substance_02 -> substance_04 in compartment_02	(.p=119.56 .max_rate=8406.84 .ratio=137.742 .product_fraction=0.921682)
	           react: substance_04 -> substance_05 in compartment_02	(.p=62.7057 .max_rate=82321.5 .product_fraction=0.594715)
	           react: substance_05 & metabolic_need -> substance_04 in compartment_02	(.p=14.043 .max_rate=4970.78 .ratio=139.661 .product_fraction=0.993736)
	   inverse react: glucose & fat -> substance_02 in compartment_02	(.p=9.4093 .max_rate=332838 .ratio=0.0806513)
	 inhibited react: substance_05 by metabolic_need -> substance_01 in compartment_02	(.p=149.056 .max_rate=8581.07 .ratio=115.876 .product_fraction=0.0823729)
	           react: substance_04 & substance_02 -> glucose in compartment_02	(.p=85.23 .max_rate=2801.99 .ratio=56.1266 .product_fraction=0.00238839)
	           react: substance_04 & metabolic_need -> glucose in compartment_02	(.p=26.9222 .max_rate=47212.9 .ratio=75.2966 .product_fraction=0.626069)
	           react: substance_05 & substance_02 -> glucose in compartment_02	(.p=0.001 .max_rate=2860.75 .ratio=0.00472297 .product_fraction=0.997567)
	           react: substance_05 & metabolic_need -> glucose in compartment_02	(.p=0.0357919 .max_rate=8561.94 .ratio=157.118 .product_fraction=0.831509)
	           react: fat & substance_02 -> glucose in compartment_02	(.p=0.257596 .max_rate=7604.4 .ratio=17.77 .product_fraction=0.945195)
	           react: fat & metabolic_need -> glucose in compartment_02	(.p=30.4495 .max_rate=5866.41 .ratio=3.05108 .product_fraction=0.9921)
	           react: protein -> substance_05 in compartment_02	(.p=149.517 .max_rate=323.965 .product_fraction=0.31466)
	 inhibited react: glucose by substance_01 -> fat in compartment_02	(.p=137.146 .max_rate=293236 .ratio=142.687 .product_fraction=0.886369)
	       transport: substance_01 in compartment_02 -> dev_null	(.p=114.949 .max_rate=112.141)
	       transport: substance_02 in compartment_02 -> dev_null	(.p=75.1001 .max_rate=8831.82)
	       transport: substance_04 in compartment_02 -> dev_null	(.p=145.346 .max_rate=90661.2)
	       transport: substance_05 in compartment_02 -> dev_null	(.p=62.9519 .max_rate=11.1242)
	       transport: fat in compartment_02 -> dev_null	(.p=186.306 .max_rate=4263.7)
	       transport: protein in compartment_02 -> dev_null	(.p=175.398 .max_rate=7736.5)
	        equalize: glucose in compartment_01 <-> sensor_tissue	(.p=38.6902 .max_rate=1211.05)
	        equalize: substance_01 in compartment_01 <-> sensor_tissue	(.p=0.00385207 .max_rate=225.915)
	        equalize: substance_05 in compartment_01 <-> sensor_tissue	(.p=11.7059 .max_rate=3991.1)
	        equalize: glucose in compartment_01 <-> compartment_02	(.p=2.59498 .max_rate=4795.16)
	        equalize: substance_01 in compartment_01 <-> compartment_02	(.p=0.111119 .max_rate=733.701)
	        equalize: substance_05 in compartment_01 <-> compartment_02	(.p=0.042215 .max_rate=8566.24)
	        equalize: protein in compartment_01 <-> compartment_02	(.p=0.57511 .max_rate=4470.85)
	        equalize: fat in compartment_01 <-> compartment_02	(.p=4.04034 .max_rate=13563.7)
.end metabolism

.signals begin
	 emit: glucose in sensor_tissue as {B44F2278-F592-491E-BE20-6DF6999AC2D8}
	 consume: {37AA6AC1-6984-4A06-92CC-A660110D0DC7} as glucose in digestion (.conversion_factor=0.6167498460116704)
	 consume: {A307E993-1318-415F-ABFC-BCE0E0E8E431} as protein in digestion (.conversion_factor=0.07996404252837852)
	 consume: {FF4A88BB-979C-44E3-865C-1FD592847ED4} as fat in digestion (.conversion_factor=10.4082215929169)
	 consume: {6DFCFD02-C48C-4CE0-BD82-2D941E767A99} as metabolic_need in compartment_02 (.conversion_factor=0.004577136348135813 .sensor=true)
.end signals

.state begin
	 init: 1.65946 -> fat in compartment_01
	 init: 8.16653 -> glucose in compartment_01
	 init: 0.708167 -> protein in compartment_01
	 init: 0.0689483 -> substance_01 in compartment_01
	 init: 0.359227 -> substance_05 in compartment_01
	 init: 0.388173 -> fat in compartment_02
	 init: 1.4001 -> glucose in compartment_02
	 init: 0.45 -> metabolic_need in compartment_02
	 init: 0.174328 -> protein in compartment_02
	 init: 0.147002 -> substance_01 in compartment_02
	 init: 0.00304811 -> substance_02 in compartment_02
	 init: 66.049 -> substance_03 in compartment_02
	 init: 9.33734e-13 -> substance_04 in compartment_02
	 init: 0.120675 -> substance_05 in compartment_02
	 init: 0.549477 -> fat in digestion
	 init: 1.29298 -> glucose in digestion
	 init: 0.753975 -> protein in digestion
	 init: 9.74099 -> glucose in sensor_tissue
	 init: 0.113161 -> substance_01 in sensor_tissue
	 init: 0.244058 -> substance_05 in sensor_tissue
.end state

.global begin
	init: 10 -> stepping //seconds
.end global
