A training dataset is used to determine parameters of a particular metabolism. If such a training dataset is smaller than the original, input dataset, then it is possible to verify correctness of the determined parameters by observing the simulated metabolism, when it reacts to previously unseen events. If the simulation provides correct output within desired error range, then it is a proof that the procedure (used to determine the parameters) is correct. For this dataset, 70% of the original data formed the training dataset, while the entire dataset was used to compute the reported error metrics.


Each folder represents a single patient. Particularly, these patients demonstrate Sirael's ability to model glucose, protein, fat and fiber metabolism. In each folder, there are multiple profiles - parameters to configure the Siraela metabolic machine. For the simulation, you need to feed Siraela with carbohydrates, proteins, fat, protein and heartbeat. Then, it will provide simulated blood and interstitial-fluid glucose levels.

The plot and error-report figure demonstrate error of each profile from the original data, the profile was fitted to. The original data are not available here due to copyright issues. If you want to verify, you have to download and parse them separately for yourself.

The source of the data is the "BIG IDEAs Lab Glycemic Variability and Wearable Device Data" dataset, but the analyses, content and conclusions presented herein are solely the responsibility of the authors and have not been reviewed or approved by the study authors.



Use Siraela Terminal from https://gitlab.com/sirael_metabolic_simulator/siraela to execute the simulations.

Files
=====
*.sirael - this is the metabolism description file
*.sir - this the compiled .sirael file, i.e.; an intermediate representation between the actual simulation and the metabolism user-friendly description
*.svg - this file depicts the simulation including error quantification
*.scenario.txt - this is a simulation scenario
*.sign.base64 - digital signature


Warning!!!
=========

In-silico simulation is a very complicated task. There are many effects, which seem to be counter-intuitive. For example, the dawn phenomenon is a an-early morning hyperglycemia after hours without food. It can be explained, but not in a simple way. Simulating the pre-diabetes condition is more challenging than simulating Type-1 Diabetes as the internal organs are still functioning, but they already exhibit patterns of their future malfunctioning. This leads to smaller changes in the glucose level and brings additional components to the entire simulation.


Due to phenomena like these, all the Siraela metabolic programs are digitally signed so that you can verify if they come intact from this repository. Although it does not mean that anyone else could compute better parameters, it means, for sure, that someone took the best available knowledge of Sirael to make sure that the presented parameters are as much physiologically plausible as possible with the current level of knowledge. Simply said, it is too easy to compute different set of parameters with much less relative error for the known sequence of events, but with totally implausible behavior for a different set of meals, activity, etc.


Note that this pre-diabetes population serves as a proof that Sirael is not limited to Type-1 diabetes and as a proof that it can handle a realistic meal composition. Be aware that some of the original metabolic programs were irreversibly transformed from a purely physiological description, to reduce their computational complexity, while preserving the physiologically-correct responses. As simulating full-meal composition is very complex, and out of capability of other simulators, some in-silico patients are not fully adapted to all meal ingredients. Mostly, they miss the fiber effect.



BIG IDEAs Lab Glycemic Variability and Wearable Device Data
===========================================================
Cho, P., Kim, J., Bent, B., & Dunn, J. (2023). BIG IDEAs Lab Glycemic Variability and Wearable Device Data (version 1.1.2). PhysioNet. https://doi.org/10.13026/zthx-5212.
Bent, B., Cho, P.J., Henriquez, M. et al. Engineering digital biomarkers of interstitial glucose from noninvasive smartwatches. npj Digit. Med. 4, 89 (2021). https://doi.org/10.1038/s41746-021-00465-w
Goldberger, A., Amaral, L., Glass, L., Hausdorff, J., Ivanov, P. C., Mark, R., Stanley, H. E. (2000). PhysioBank, PhysioToolkit, and PhysioNet: Components of a new research resource for complex physiologic signals. Circulation [Online]. 101 (23), pp. e215–e220.